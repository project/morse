Morse
--------------------------

Install
-------
Steps :
1. Download and extract the module into modules/ directory.
2. Enable the module from admin/modules using admin login.
3. You are ready to convert the alphanumeric letters into morse code.
4. There will be menu item available in drupal navigation "Morse Converter".
5. In case you are not able to see the menu you can go to "yoursite.com/morse" to convert the code into morse.
6. To convert morse code into alphanumeric code you need to visit "yoursite.com/morse/alpha".

Description
-----------
This module is basically used to convert the alphanumeric letters into
morse code. Morse code also can be converted back into alphanumeric letters.
